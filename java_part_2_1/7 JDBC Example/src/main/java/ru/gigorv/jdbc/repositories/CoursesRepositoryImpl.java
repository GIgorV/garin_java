package ru.gigorv.jdbc.repositories;

import ru.gigorv.jdbc.models.Course;
import ru.gigorv.jdbc.models.Lesson;
import ru.gigorv.jdbc.utils.jdbc.JdbcUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.*;
import java.util.Map;

public class CoursesRepositoryImpl implements CoursesRepository {

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select c.*, l.id as lesson_id, l.name " +
            "from course c left join lesson l on c.id = l.course_id " +
            "where c.id = ?;";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select c.*, l.id as lesson_id, l.name " +
            "from course c join lesson l on c.id = l.course_id;";

    public CoursesRepositoryImpl(Connection connection) {
        this.connection = connection;
        this.jdbcUtil = new JdbcUtil(connection);
    }

    private Connection connection;
    private JdbcUtil jdbcUtil;
    private Map<Integer, Course> courseMap;

    private static RowMapper<Course> baseCourseRowMapper = row ->
            Course.builder()
                    .id(row.getInt("id"))
                    .title(row.getString("title"))
                    .lessons(new ArrayList<>())
                    .build();

    private static RowMapper<Lesson> courseLessonRowMapper = row ->
            Lesson.builder()
                    .id(row.getInt("lesson_id"))
                    .name(row.getString("name"))
                    .build();

    static RowMapper<Course> courseWithLessonsRowMapper = row -> {
        Course course = baseCourseRowMapper.mapRow(row);
        row.getInt("lesson_id");
        if (!row.wasNull()) {
            Lesson lesson = courseLessonRowMapper.mapRow(row);
            course.getLessons().add(lesson);
            while (row.next()) {
                lesson = courseLessonRowMapper.mapRow(row);
                course.getLessons().add(lesson);
            }
        }
        return course;
    };

    private RowMapper<Course> coursesWithLessonsRowMapper = row -> {
        Integer courseId = row.getInt("id");
        if (!courseMap.containsKey(courseId)) {
            Course course = baseCourseRowMapper.mapRow(row);
            courseMap.put(courseId, course);
        }
        row.getInt("lesson_id");
        if (!row.wasNull()) {
            Lesson lesson = courseLessonRowMapper.mapRow(row);
            courseMap.get(courseId).getLessons().add(lesson);
        }
        return null;
    };

    @Override
    public Optional<Course> findOneById(Integer id) {
        return jdbcUtil.findOnebyId(SQL_SELECT_BY_ID, id, courseWithLessonsRowMapper);
    }

    @Override
    public List<Course> findAll() {
        try {
            courseMap = new HashMap<>();

            List<Course> result;
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);

            while (resultSet.next()) {
                coursesWithLessonsRowMapper.mapRow(resultSet);
            }

            result = new ArrayList<>(courseMap.values());
            statement.close();
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public void save(Course object) {

    }

    @Override
    public void update(Course object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Course find(Integer id) {
        return null;
    }
}
