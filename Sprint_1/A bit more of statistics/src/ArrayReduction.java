import java.util.Arrays;

public class ArrayReduction {
    public int[] reduction(int[] array, int max){
        for (int i = 0; i < array.length; i++) { //нужно сократить массив повторений на сохраненный в result элемент
            if (array[i] == max) {
                array[i] = array[array.length - 1];
                array = Arrays.copyOf(array, array.length);
            }
        }
        return array;
    }
}
