package ru.gigorv.jdbc.repositories;

import ru.gigorv.jdbc.models.Student;

public interface StudentsRepository extends CrudRepository<Student> {
    Student findByFirstName(String firstName);
}
