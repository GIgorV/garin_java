package ru.gigorv.jdbc.models;

import lombok.*;

import java.util.List;

@Builder
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor


public class Course {
    private Integer id;
    private String title;
    private List<Lesson> lessons;
}
