package ru.gigorv.threads.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.ArrayList;
import java.util.List;

@Parameters(separators = "=")
class Arguments{
    @Parameter(names="--directory", description = "The folders")
    public List<String> folders = new ArrayList<>();
}
