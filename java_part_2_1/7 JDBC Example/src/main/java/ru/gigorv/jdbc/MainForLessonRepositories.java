package ru.gigorv.jdbc;

import ru.gigorv.jdbc.models.Course;
import ru.gigorv.jdbc.models.Lesson;
import ru.gigorv.jdbc.repositories.CoursesRepository;
import ru.gigorv.jdbc.repositories.CoursesRepositoryImpl;
import ru.gigorv.jdbc.repositories.LessonsRepository;
import ru.gigorv.jdbc.repositories.LessonsRepositoryImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Optional;

public class MainForLessonRepositories {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "Kwerty007";

    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        LessonsRepository repository = new LessonsRepositoryImpl(connection);

        Optional<Lesson> lesson = repository.findByOneId(1);
        System.out.println(lesson);
        System.out.println("-------");
        //List<Course> courses = repository.findAll();
        //System.out.println(courses);

    }
}
