import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Statistics statistics = new Statistics();
        int min;
        int countWeek = 1;
        int[] weeks = new int[18];
        Scanner scanner = new Scanner(System.in);
        while (countWeek <= 18) {
            min = 9;
            System.out.println("Введите через пробел контрольные оценки недели");
            int[] assessmentsOfCurrentWeek = new int[5];
            assessmentsOfCurrentWeek[0] = scanner.nextInt();
            if (assessmentsOfCurrentWeek[0] != 42) {
                for (int i = 1; i < 5; i++) {
                    assessmentsOfCurrentWeek[i] = scanner.nextInt();
                }
                for (int j = 0; j < assessmentsOfCurrentWeek.length; j++) {
                    if (assessmentsOfCurrentWeek[j] < min) {
                        min = assessmentsOfCurrentWeek[j];
                    }
                }
                weeks[countWeek] = min;
            } else {
                statistics.print(weeks);
                System.exit(0);
            }
            countWeek++;
        }
        statistics.print(weeks);
    }
}

