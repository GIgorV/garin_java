package ru.itdrive.filetest.utils;
import java.io.File;

public class FileTest {
	public void test(String directory){
		File dir = new File(directory);
		if(dir.exists()){
			File[] list= dir.listFiles();
			System.out.println("В указанной директории найдены следующие файлы: ");
			for (int i=0; i<list.length; i++){
				if(list[i].isFile()){
					File file = list[i];
					System.out.println(file.getName() + " размер: " + file.length() + " байт");
				}
			}
		}else{
			System.out.println("Указанного директория не существует или в нем нет файлов");
		}
	}
}