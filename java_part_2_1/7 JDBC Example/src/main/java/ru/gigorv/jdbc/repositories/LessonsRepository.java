package ru.gigorv.jdbc.repositories;

import ru.gigorv.jdbc.models.Lesson;

import java.util.List;
import java.util.Optional;

public interface LessonsRepository extends CrudRepository<Lesson> {
    Optional<Lesson> findByOneId(Integer id);
    //List<Lesson> findAll();
}
