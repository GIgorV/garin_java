package ru.gigorv.jdbc.repositories;

import ru.gigorv.jdbc.models.Course;

import java.util.List;
import java.util.Optional;

public interface CoursesRepository extends CrudRepository<Course>{
    Optional<Course> findOneById(Integer id);
    //List<Course> findAll();
}
