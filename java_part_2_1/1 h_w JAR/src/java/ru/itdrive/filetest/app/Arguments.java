package ru.itdrive.filetest.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
class Arguments{
	@Parameter(names={"--directory"})
	public String directory;
}