package ru.gigorv.jdbc.utils.jdbc;

import ru.gigorv.jdbc.repositories.RowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class JdbcUtil {
    private Connection connection;

    public JdbcUtil(Connection connection) {
        this.connection = connection;
    }

    public <T> Optional<T> findOnebyId(String sql, Integer id, RowMapper<T> rowMapper) {
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                return Optional.of(rowMapper.mapRow(resultSet));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
