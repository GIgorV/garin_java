package ru.gigorv.jdbc.repositories;

import ru.gigorv.jdbc.models.Course;
import ru.gigorv.jdbc.models.Lesson;
import ru.gigorv.jdbc.utils.jdbc.JdbcUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class LessonsRepositoryImpl implements LessonsRepository {


    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select l.id as lesson_id, l.name, c.id, c.title " +
            "from lesson l join course c on l.course_id = c.id " +
            "where l.id = ?;";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select l.*, c.title " +
            "from lesson l join course c on l.course_id = c.id;";

    public LessonsRepositoryImpl(Connection connection) {
        this.connection = connection;
        this.jdbcUtil = new JdbcUtil(connection);
    }

    private Connection connection;
    private JdbcUtil jdbcUtil;
    private Map<Integer, Lesson> lessonMap;

    private static RowMapper<Lesson> lessonRowMapper = row -> {
        row.getInt("id");
        Course course = null;
        if (!row.wasNull()) {
     //       course = CoursesRepositoryImpl.courseWithLessonsRowMapper.mapRow(row);
            course = Course.builder().id(row.getInt("id")).title(row.getString("title")).build();
        }
        return Lesson.builder()
                .id(row.getInt("lesson_id"))
                .name(row.getString("name"))
                .course(course)
                .build();
    };

    @Override
    public Optional<Lesson> findByOneId(Integer id) {
        return jdbcUtil.findOnebyId(SQL_SELECT_BY_ID, id, lessonRowMapper);
    }

    @Override
    public List<Lesson> findAll() {
/*
        try {
            lessonMap = new HashMap<>();

            List<Lesson> result;
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);

            while (resultSet.next()) {
                coursesWithLessonsRowMapper.mapRow(resultSet);
            }

            result = new ArrayList<>(lessonMap.values());
            statement.close();
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
*/
        return null;
    }

    /*@Override
    public Lesson findByName(String name) {
        return null;
    }*/

    @Override
    public void save(Lesson object) {

    }

    @Override
    public void update(Lesson object) {

    }

    @Override
    public void delete(Integer id) {

    }
    @Override
    public Lesson find(Integer id) {
        return null;
    }
}
