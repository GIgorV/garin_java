package ru.gigorv.jdbc.models;

import lombok.*;

import java.util.List;

@Builder
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Lesson {
    private Integer id;
    private String name;
    private Course course;
}