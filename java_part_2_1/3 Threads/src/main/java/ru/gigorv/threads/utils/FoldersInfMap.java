package ru.gigorv.threads.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FoldersInfMap {
    HashMap<String, List<String>> map = new HashMap<>();

    public void setMap(String folderName, List<String> listFileInformation) {
        map.put(folderName, listFileInformation);
    }

    public void print(){
        for (Map.Entry<String,List<String>> entry : map.entrySet()) {
            System.out.print(" " + entry.getKey() + " " + entry.getValue());
            System.out.println();
        }
    }
}
//    private String folderName;
//    private List<String> listFileInformation;

//    public String getFolderName() {
//        return folderName;
//    }
//
//    public void setFolderName(String folderName) {
//        this.folderName = folderName;
//    }
//
//    public List<String> getListFileInformation() {
//        return listFileInformation;
//    }
//
//    public void setListFileInformation(List<String> listFileInformation) {
//        this.listFileInformation = listFileInformation;
//    }
//
//        public HashMap<String, List<String>> getMap() {
//        return map;
//    }
//
//    public void setMap(HashMap<String, List<String>> map) {
//        this.map = map;
//    }


