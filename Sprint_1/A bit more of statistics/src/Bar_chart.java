import java.util.Arrays;

public class Bar_chart {
    public void counting(String arbitrary) {
        char[] text = arbitrary.toCharArray();
        // меняем вручную заглавные буквы на строчные
        for (int i = 0; i < text.length; i++) {
            int codeOfAscii = text[i];
            if (codeOfAscii < 97 && codeOfAscii != 32) {
                codeOfAscii = codeOfAscii + 32;
                text[i] = (char) codeOfAscii;
            }
        }


        int count[] = new int[26]; // массив для подсчета повторений (букв англ. алфавита)
        for (int i = 0; i < text.length; i++) {
            if ((int) text[i] != 32 || (int) text[i] != 44) {
                int reiteration = (int) text[i] - 97; // индекс текущего символа - 97 = индекс массива повторений,
                // где reiteration является ссылкой на букву.
                count[reiteration]++; //получили массив повторений, где индекс - буква, а значение - количество попаданий
            }
        }
//        for (int i = 0; i < count.length; i++) {
//            System.out.print(count[i] + " ");
//        }

        int[] countCopy = new int[count.length];
        System.arraycopy(count, 0, countCopy, 0, count.length);

//        for (int i = 0; i < countCopy.length; i++) {
//            System.out.print(countCopy[i] + " ");
//        }

        String result = "";
        int current = 0;
        int max;
        int countA = 0; //для того, чтобы не повторялась 'a', т.к. max будет 0 в цикле
        char currentLetter;

        while (current < 26 ^ countA > 0) {
            max = count[0];
            for (int i = 1; i < count.length; i++) {    //ищем максимальный "символ"
                if (count[i] > max) {
                    max = count[i];
                }
            }

            for (int i = 0; i < count.length; i++) { // формируем результирующую строку от большего к меньшему
                if (count[i] == max) {
                    currentLetter = (char) (i + 97);
                    if (currentLetter == 'a') {
                        countA++;
                    }
                    result += String.valueOf(currentLetter);
                    result += " ";
                }
            }


            for (int i = 0; i < count.length; i++) { //нужно сократить массив повторений на сохраненный в result элемент
                if (count[i] == max) {
                    count[i] = count[count.length - 1];
                    count = Arrays.copyOf(count, count.length);
                }
            }
            current++;
        }

        current = 0;
        String string = "";

        while (current < result.length()/2)

        {
            max = 0;
            for (int i = 0; i < countCopy.length; i++) {
                if (countCopy[i] > max) {
                    max = countCopy[i];
                }
            }
            System.out.println(string + max);
            string += "#";
            ArrayReduction arrayReduction = new ArrayReduction();
            countCopy = arrayReduction.reduction(countCopy, max);
            current++;
        }

        System.out.println(result); //последняя


    }
}
// убираем нули с конца (https://www.codeflow.site/ru/article/java-remove-trailing-characters)
//    StringBuilder sb = new StringBuilder(result);
//        while (sb.length()>0 && sb.charAt(sb.length()-1) == '0'){
//                sb.setLength(sb.length()-1);
//                }
//                result = sb.toString();
