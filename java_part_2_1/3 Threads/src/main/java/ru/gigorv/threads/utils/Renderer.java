package ru.gigorv.threads.utils;

import java.io.File;
import java.util.*;

public class Renderer extends Thread {
    String folderName;
    List<String> listFileInformation = new ArrayList<>();
    FoldersInfMap foldersInfMap;

    public Renderer(String folderName, FoldersInfMap foldersInfMap) {
        this.folderName = folderName;
        this.foldersInfMap = foldersInfMap;
    }


    @Override
    public void run() {
        File dir = new File(folderName);
        if (dir.exists()) {
            File[] list = dir.listFiles();
            for (int i = 0; i < list.length; i++) {
                if (list[i].isFile()) {
                    File file = list[i];
                    String fileInformation = file.getName() + ", " + file.length();
                    listFileInformation.add(fileInformation);
                }
            }
        } else {
            System.out.println("Путь " + folderName + " не существует или в нем нет файлов");
        }
        foldersInfMap.setMap(folderName, listFileInformation);
    }
}

