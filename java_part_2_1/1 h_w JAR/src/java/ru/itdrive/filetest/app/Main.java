package ru.itdrive.filetest.app;
import ru.itdrive.filetest.utils.FileTest;
import com.beust.jcommander.JCommander;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
    	Arguments arguments = new Arguments();
		//далее мы переносим параметры из массива коммандной строки args  в объект класса arguments
		JCommander.newBuilder()
			.addObject(arguments)
			.build()
			.parse(args);
        FileTest fileTest = new FileTest();
        fileTest.test(arguments.directory);
    }
}
