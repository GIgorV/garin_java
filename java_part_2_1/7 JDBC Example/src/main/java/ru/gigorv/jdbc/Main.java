package ru.gigorv.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class Main {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "Kwerty007";

    //language=SQL
    private static final String SQL_SELECT_STUDENT = "select * from student";

    //language=SQL
    private static final String SQL_SELECT_STUDENT_BY_ID = "select * from student where id = ";

    public static void main(String[] args) throws Exception {
        Class.forName("org.postgresql.Driver"); //в явном виде загружает класс драйвера
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(SQL_SELECT_STUDENT);
        //statement - это интерфейс,
        // возвращает ResultSet - тоже интерфейс для получения данных из конкретной строки, это итератор,
        // поэтому ему нужно задать позицию:

        result.next(); //указывает на строку
        System.out.print(result.getString("first_name") + " "); //get вызывает определенные столбцы
        System.out.println(result.getInt("age"));
        result.next();
        System.out.print(result.getString("first_name") + " ");
        System.out.println(result.getInt("age"));

        //новый ResultSet с определенный запросом:
        ResultSet anotherResult = statement.executeQuery(SQL_SELECT_STUDENT_BY_ID + " 3");
        anotherResult.next();
        System.out.println(anotherResult.getString("first_name") + " " +
                anotherResult.getString("last_name"));

        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        String insertSql = "insert into student(first_name) values ('" + name + "')";
        //сколько строк было обновлено
        int affectedRows = statement.executeUpdate(insertSql);
        System.out.println(affectedRows);

    }
}
