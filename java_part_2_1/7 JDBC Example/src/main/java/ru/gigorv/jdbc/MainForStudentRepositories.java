package ru.gigorv.jdbc;

import ru.gigorv.jdbc.models.Student;
import ru.gigorv.jdbc.repositories.StudentsRepository;
import ru.gigorv.jdbc.repositories.StudentsRepositoryJdbcImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

public class MainForStudentRepositories {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "Kwerty007";

    public static void main(String[] args) throws Exception {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        StudentsRepository repository = new StudentsRepositoryJdbcImpl(connection);

        Student student = repository.find(1);
        System.out.println(student);
        System.out.println("-------");
        List<Student> students = repository.findAll();
        System.out.println(students);

    }
}
