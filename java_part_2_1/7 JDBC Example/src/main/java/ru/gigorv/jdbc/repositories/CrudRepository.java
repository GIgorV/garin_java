package ru.gigorv.jdbc.repositories;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {
    void save(T object);
    void update(T object);
    void delete(Integer id);
    T find(Integer id);

    List<T> findAll();
}
