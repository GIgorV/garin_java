package ru.gigorv.threads.app;

import com.beust.jcommander.JCommander;
import ru.gigorv.threads.utils.FoldersInfMap;
import ru.gigorv.threads.utils.Renderer;

import java.util.List;

public class Program {
    public static void main(String[] args) {
        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        FoldersInfMap foldersInfMap = new FoldersInfMap();
        List<String> folders = arguments.folders;
        Renderer[] threads = new Renderer[folders.size()];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Renderer(folders.get(i), foldersInfMap);
        }
        for (Renderer thread : threads) {
            thread.start();
        }
        for (Renderer thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        foldersInfMap.print();
    }
}