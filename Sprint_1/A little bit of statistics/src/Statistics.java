
class Statistics {
    public void print(int[] weeks) {
        for (int i = 0; i < weeks.length; i++) {
            if (weeks[i] == 1) {
                System.out.println("=>");
            }
            if (weeks[i] == 2) {
                System.out.println("==>");
            }
            if (weeks[i] == 3) {
                System.out.println("===>");
            }
            if (weeks[i] == 4) {
                System.out.println("====>");
            }
            if (weeks[i] == 5) {
                System.out.println("=====>");
            }
            if (weeks[i] == 6) {
                System.out.println("======>");
            }
            if (weeks[i] == 7) {
                System.out.println("=======>");
            }
            if (weeks[i] == 8) {
                System.out.println("========>");
            }
            if (weeks[i] == 9) {
                System.out.println("=========>");
            }
        }
    }
}