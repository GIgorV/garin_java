package ru.gigorv.jdbc.models;

import lombok.*;

import java.util.List;

@Builder
//@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Student {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
    private Boolean isActive;


    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isActive=" + isActive +
                '}';
    }
}
